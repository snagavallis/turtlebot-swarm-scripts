#!/bin/bash

##############################################################################
# File        : install_ros_indigo.bash
# Description : Installs ROS Indigo on Ubuntu 14.04 LTS
# Author      : Sasanka Nagavalli 
#             : Based on intructions found here: 
#             :   http://wiki.ros.org/indigo/Installation/Ubuntu
# Date        : January 20, 2016
##############################################################################

# Ensure everything is currently up to date
sudo apt-get update
sudo apt-get -y dist-upgrade
sudo apt-get -y install openssh-server vim git python-pip

# Accept software from packages.ros.org
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'

# Add keys
sudo apt-key adv --keyserver hkp://pool.sks-keyservers.net --recv-key 0xB01FA116

# Install ROS
sudo apt-get update
sudo apt-get -y install ros-indigo-desktop-full python-rosinstall 

# Initialize rosdep
sudo rosdep init
rosdep update

# Update environment for ROS
source /opt/ros/indigo/setup.bash

# Set up catkin_ws
mkdir -p ~/catkin_ws/src
cd ~/catkin_ws/src
catkin_init_workspace
cd ~/catkin_ws
catkin_make

# Update environment for catkin
source ~/catkin_ws/devel/setup.bash

# Install additional packages for TurtleBots
sudo apt-get -y install ros-indigo-rocon ros-indigo-turtlebot ros-indigo-turtlebot-concert ros-indigo-turtlebot-apps ros-indigo-turtlebot-interactions ros-indigo-turtlebot-simulator ros-indigo-rocon-remocon ros-indigo-rocon-qt-library ros-indigo-ar-track-alvar-msgs 

# Disable avahi-daemon from detecting local and stopping
sudo sed -i 's/^AVAHI_DAEMON_DETECT_LOCAL=1/AVAHI_DAEMON_DETECT_LOCAL=0/g' /etc/default/avahi-daemon

# Done
sudo reboot
exit 0
