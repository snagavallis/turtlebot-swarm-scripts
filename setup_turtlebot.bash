#!/bin/bash

##############################################################################
# File        : setup_turtlebot.bash
# Description : Additional setup for a TurtleBot (after ROS installation)
# Author      : Sasanka Nagavalli 
# Date        : January 20, 2016
##############################################################################

# Install TurtleBot packages
sudo apt-get -y install ros-indigo-turtlebot ros-indigo-turtlebot-apps ros-indigo-turtlebot-interactions ros-indigo-turtlebot-simulator ros-indigo-kobuki-ftdi ros-indigo-rocon-remocon ros-indigo-rocon-qt-library ros-indigo-ar-track-alvar-msgs ros-indigo-apriltags ros-indigo-apriltags-ros ros-indigo-usb-cam

# Enable access to serial ports
sudo adduser turtlebot dialout

# Install package to control BlinkStick (USB LED)
sudo pip install blinkstick
sudo blinkstick --add-udev-rule

# Don't suspend or shutdown if lid is closed
gsettings set org.gnome.settings-daemon.plugins.power lid-close-ac-action 'nothing'
gsettings set org.gnome.settings-daemon.plugins.power lid-close-battery-action 'nothing'

# Disable ads
gsettings set com.canonical.Unity.Lenses remote-content-search 'none'

# Reboot for blinkstick udev rule
sudo reboot
exit 0
