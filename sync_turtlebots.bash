#!/bin/bash

##############################################################################
# File        : sync_turtlebots.bash
# Description : Sync catkin_ws of all turtlebots
# Author      : Sasanka Nagavalli 
# Date        : February 28, 2016
##############################################################################

DEST_CATKIN_WS=/home/turtlebot/catkin_ws

SRC_PACKAGE_PATH=~/turtlebot-swarm-myo
DEST_PACKAGE_PATH=${DEST_CATKIN_WS}/src

TURTLEBOTS="turtlebot05 turtlebot06 turtlebot07 turtlebot08 turtlebot09"
PACKAGES="blinkstick_ros turtlebot_swarm_rapps"

for TURTLEBOT in ${TURTLEBOTS}
do 
  for PACKAGE in ${PACKAGES}
  do
    rsync -avz --del \
      ${SRC_PACKAGE_PATH}/${PACKAGE} \
      turtlebot@${TURTLEBOT}:${DEST_PACKAGE_PATH} &
  done
done 
wait

for TURTLEBOT in ${TURTLEBOTS}
do 
  ssh turtlebot@${TURTLEBOT} -t -t "bash -ic \"cd ${DEST_CATKIN_WS}; catkin_make\"" & 
done
wait

exit 0
