#!/bin/bash

##############################################################################
# File        : make_pxelinux_cfg.bash
# Description : Create a boot menu for network installation
# Author      : Sasanka Nagavalli 
# Date        : January 23, 2016
##############################################################################

: ${1?"Usage: $0 tftp_server"}
tftp_server="$1"

echo "menu title Magic Turtlebot Installer" 
echo "default manual"
manual_menu_entry="label manual
	menu label ^manual
	kernel ubuntu-installer/i386/linux
	append initrd=ubuntu-installer/i386/initrd.gz --"
echo "${manual_menu_entry}"
for turtlebot_id in `seq 0 9`
do 
  turtlebot_name="turtlebot0${turtlebot_id}"
  turtlebot_menu_entry="label ${turtlebot_name}
	menu label ^${turtlebot_name}
	kernel ubuntu-installer/i386/linux
	append auto=true locale=en_US console-setup/ask_detect=false console-setup/layoutcode=us interface=auto hostname=${turtlebot_name} domain= url=tftp://${tftp_server}/preseed/turtlebot.cfg initrd=ubuntu-installer/i386/initrd.gz --"
  echo "${turtlebot_menu_entry}"
done
echo "default ubuntu-installer/i386/boot-screens/vesamenu.c32" 
echo "prompt 1" 
echo "timeout 300" 

exit 0
