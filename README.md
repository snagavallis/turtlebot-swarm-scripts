## Description

This repository includes scripts to automate installation and bringup of multiple Turtlebots. 

## Automatic OS Installation via Network 

### Requirements

1. TFTP server (e.g. tftpd-hpa)
2. DHCP server (e.g. Asus RT-AC66U with DD-WRT installed and DNSMasq enabled)
3. Turtlebot computer that supports PXE network boot (e.g. Asus Eee PC 1025c)

### Instructions 

#### TFTP Server

1. Get Ubuntu 14.04 LTS [netboot](http://cdimage.ubuntu.com/netboot/14.04/) image (`netboot.tar.gz`).

		$ wget http://archive.ubuntu.com/ubuntu/dists/trusty-updates/main/installer-i386/current/images/netboot/netboot.tar.gz

2. Unpack in TFTP server root directory (e.g. `/var/lib/tftpboot`)

		$ sudo tar zxvf netboot.tar.gz -C $TFTP_ROOT_DIR

3. Create new Turtlebot installer menu.

		$ turtlebot-swarm-scripts/make_pxelinux_cfg.bash > turtlebot_menu.cfg
		$ sudo cp turtlebot_menu.cfg $TFTP_ROOT_DIR/pxelinux.cfg/default 

4. Copy the preseed file for Turtlebot installation.
	
		$ sudo mkdir -p $TFTP_ROOT_DIR/preseed
		$ sudo cp turtlebot-swarm-scripts/turtlebot.cfg $TFTP_ROOT_DIR/preseed

#### DHCP Server

1. Configure the DHCP server to allow booting from TFTP server. For example, if your router has DD-WRT installed with DNSMasq enabled and your TFTP server has IP `192.168.1.2`, then add the following line to your DNSMasq options:

		dhcp-boot=pxelinux.0,"192.168.1.2",192.168.1.2
	
#### Turtlebot Computer 

0. Connect Turtlebot computer to network.
1. Enable PXE / network boot for the connected network interface in BIOS.
2. Select the network boot option at boot time (e.g. Atheros Boot Agent).
3. You should see the Turtlebot installer menu. Select the desired option to install OS.
4. After installation, reboot from hard drive. Change password (`letmein`) on first boot.  
5. Clone this repository. Run scripts.

		$ turtlebot-swarm-scripts/install_ros_indigo.bash
		$ turtlebot-swarm-scripts/setup_turtlebot.bash
		$ turtlebot-swarm-scripts/append_bashrc.bash
		
6. Edit `~/.bashrc` as required (`ROS_HOSTNAME`, `TURTLEBOT_NAME`, etc.). 
