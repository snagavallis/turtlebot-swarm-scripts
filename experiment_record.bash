#!/bin/bash

##############################################################################
# File        : experiment_record.bash
# Description : Record experimental data
# Author      : Sasanka Nagavalli 
# Date        : February 29, 2016
##############################################################################

topics="/tag_detections /behavior /leader"

for camera_name in blue_camera yellow_camera green_camera red_camera
do
  topics="${topics} /${camera_name}/tag_detections"
  topics="${topics} /${camera_name}/tag_detections_image"
  topics="${topics} /${camera_name}/usb_cam/camera_info"
done

for myo_name in leader_myo behavior_myo
do
  topics="${topics} /${myo_name}/gesture"
done

for turtlebot_id in `seq 5 9`
do
  topics="${topics} /turtlebot0${turtlebot_id}/status"
  topics="${topics} /turtlebot0${turtlebot_id}/teleop/cmd_vel"
  topics="${topics} /turtlebot0${turtlebot_id}/teleop/css_color"
  topics="${topics} /turtlebot0${turtlebot_id}/teleop/odom_combined"
done 

rosbag record ${topics}

exit 0
