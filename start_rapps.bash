#!/bin/bash

##############################################################################
# File        : start_rapps.bash
# Description : Script to start a rapp on several TurtleBots
# Author      : Sasanka Nagavalli 
# Date        : January 20, 2016
##############################################################################

TURTLEBOTS="turtlebot05 turtlebot06 turtlebot07 turtlebot08 turtlebot09"
RAPP="turtlebot_swarm_rapps/auto_led_teleop"

for turtlebot in ${TURTLEBOTS}
do
  remappings="{remap_from: /teleop/cmd_vel, remap_to: /${turtlebot}/teleop/cmd_vel}, {remap_from: /teleop/odom_combined, remap_to: /${turtlebot}/teleop/odom_combined}, {remap_from: /teleop/compressed_image, remap_to: /${turtlebot}/teleop/compressed_image}, {remap_from: /teleop/css_color, remap_to: /${turtlebot}/teleop/css_color}"
  parameters=""
  rosservice call /${turtlebot}/start_rapp "${RAPP}" ["${remappings}"] ["${parameters}"] &
done

wait

exit 0
