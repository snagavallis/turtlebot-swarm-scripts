#!/bin/bash

##############################################################################
# File        : setup_ssh_keys.bash
# Description : Setup SSH keys to login to TurtleBots without password
# Author      : Sasanka Nagavalli 
# Date        : January 20, 2016
##############################################################################

# Generate key
ssh-keygen -t rsa

# Copy to all TurtleBots
for turtlebot_id in `seq 0 9`
do 
  ssh-copy-id turtlebot@turtlebot0$turtlebot_id
done 

exit 0
