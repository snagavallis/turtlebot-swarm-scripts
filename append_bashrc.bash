#!/bin/bash

##############################################################################
# File        : append_bashrc.bash
# Description : Modify the .bashrc file for ROS Indigo
# Author      : Sasanka Nagavalli 
# Date        : January 20, 2016
##############################################################################

APPEND_TO_BASHRC="
source /opt/ros/indigo/setup.bash
source ~/catkin_ws/devel/setup.bash

export ROS_MASTER_URI=http://localhost:11311
export ROS_HOSTNAME=\$(hostname -I)

export TURTLEBOT_BASE=create
export TURTLEBOT_STACKS=circles
export TURTLEBOT_3D_SENSOR=kinect
export TURTLEBOT_SERIAL_PORT=/dev/ttyUSB0
export TURTLEBOT_NAME=\$(hostname)
"

echo "${APPEND_TO_BASHRC}" >> ~/.bashrc
